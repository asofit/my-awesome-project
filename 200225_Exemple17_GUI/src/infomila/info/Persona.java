/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package infomila.info;

/**
 *
 * @author Usuari
 */
public class Persona {
    private Integer edat;
    private String cognom;
    private String nom;

    public Persona(Integer edat, String cognom, String nom) {
        this.edat = edat;
        this.cognom = cognom;
        this.nom = nom;
    }

    public Integer getEdat() {
        return edat;
    }

    public void setEdat(Integer edat) {
        this.edat = edat;
    }

    public String getCognom() {
        return cognom;
    }

    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return  cognom + ", " + nom + '-' + edat;
    }
    
    
}
