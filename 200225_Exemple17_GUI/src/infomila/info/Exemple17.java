/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package infomila.info;

import java.util.ArrayList;
import javafx.scene.layout.Border;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author Usuari
 */
    public class Exemple17 extends JFrame {
    
    private JList<String> llista;
    private DefaultListModel modelLlista;
    private ArrayList<Persona> persones;
    private JPanel panellCentral;
    private JButton afegir;
    private JButton esborrar;
    private JScrollPane scroll;
    
    public Exemple17(String titol) {
        setTitle(titol);
        entornGrafic(); 
        pack();
        setVisible(true);
        setResizable(false); // no permetre modificar la mida de la finestra
        setLocation(10,300); // ubicar l'aplicació al monitor tant pixels x,y respecte el punt 0,0, superior,esquerra
        // +x, més a la dreta
        // +y, més avall
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    private void entornGrafic()
    {
        inicialitzarObjectes();
        /* TODO:
        Afegir dins els Panells i Frame els Components que calgui
        */
        Border marc = BorderFactory.createLineBorder(panellCentral.getBackground(),10);
        
        
        
    }
    
    private void inicialitzarObjectes()
    {
        persones = new ArrayList<Persona>();
        persones.add(new Persona(59,"Banderas","Antonio"));
        persones.add(new Persona(61,"Pfeiffer","Michelle"));
        persones.add(new Persona(55,"Reeves","Keanu Charles"));
        persones.add(new Persona(70,"Weaver","Sigourney"));
        persones.add(new Persona(61,"Ciccone","Madonna Louise Veronica"));
        
        modelLlista = new DefaultListModel();
        /*
            Ús de la classe DefaultListModel per poder gestionar els elements que hi hagi dins la llista fàcilment
        */
        // Initialize the list with items
        for (int i = 0; i < persones.size(); i++) {
          modelLlista.add(i, persones.get(i));
        }
        
        llista = new JList(modelLlista);
        scroll = new JScrollPane(llista);
        
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        
        // decideixo que el Central sera pels botons
        panellCentral = new JPanel();
        panellCentral.setLayout(new BoxLayout(panellCentral, BoxLayout.Y_AXIS));
        afegir = new JButton("Afegir");
        esborrar = new JButton("Esborrar");
        
        /* TODO:
        Inicialitzar Components segons correspongui, panells i botons
        En els panells, recordar d'indicar el tipus de LayoutManager si correspon
        */
    }
}
